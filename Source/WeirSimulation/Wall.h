// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
class WEIRSIMULATION_API Wall
{
public:
	FVector normal;
	float d;
	double maxX, minX;
	double maxY, minY;
	double maxZ, minZ;
	FVector sphereRadiusVec;
	/// <summary>
	/// Construct a wall by three points and a point above the plane.
	/// Keep in mind the three points can't lay on the same line 
	/// </summary>
	/// <param name="pointA">First point</param>
	/// <param name="pointB">Second point</param>
	/// <param name="pointC">Third point</param>
	/// <param name="upPoint">A point above the plane</param>
	/// <param name="s">radius of the particle sphere</param>
	Wall(FVector pointA, FVector pointB, FVector pointC, FVector upPoint,float sphereRadius);
	Wall(FVector normal, float d, float sphereRadius);
	~Wall();

	/// <summary>
	/// Sets the SphereRadius vector
	/// </summary>
	/// <param name="halfSphere">size of the particle sphere </param>
	void SetSphereRadiusVector(double halfSphere);

	/// <summary>
	/// Set the boundaries for the wall
	/// </summary>
	/// <param name="maxX">The maximum X coordinate in which the wall will interact</param>
	/// <param name="maxY">The maximum Y coordinate in which the wall will interact</param>
	/// <param name="maxZ">The maximum Z coordinate in which the wall will interact</param>
	/// <param name="minX">The minimum X coordinate in which the wall will interact</param>
	/// <param name="minY">The minimum Y coordinate in which the wall will interact</param>
	/// <param name="minZ">The minimum Z coordinate in which the wall will interact</param>
	void SetMaxMin(double maxX, double maxY, double maxZ, double minX, double minY, double minZ);

	/// <summary>
	/// Returns flipped velocity. Doesn't do any dampening or anything else
	/// </summary>
	/// <param name="vel"> the velocity of the bouncing object</param>
	/// <returns></returns>
	FVector Bounce(FVector vel);

	/// <summary>
	/// Checks if given position is below the plane
	/// </summary>
	/// <param name="pos">position of the object</param>
	/// <returns>True if it's below, False if not</returns>
	bool IsBelow(FVector pos);
	/// <summary>
	/// Checks if the intersection point is in the boundaries of the wall
	/// </summary>
	/// <param name="pos">The position of the intersection point</param>
	/// <returns></returns>
	bool ShouldInteract(FVector pos);
	/// <summary>
	/// Finds if intersection if it happened in the last iteration.
	/// </summary>
	/// <param name="pos">current position of the particle (after update so it is behined the wall)</param>
	/// <param name="moveVector">current MoveVector of the particle(by which it moved! so it had to be multiplied by TIMESTEP)</param>
	/// <returns></returns>
	FVector FindIntersection(FVector pos, FVector moveVector);

private:
	void SetMaxMinToINF();

};