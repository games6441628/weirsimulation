// Fill out your copyright notice in the Description page of Project Settings.


#include "Wall.h"
#include <cmath>



Wall::Wall(FVector pointA, FVector pointB, FVector pointC, FVector upPoint, float s) {
	FVector AB = pointB - pointA;
	FVector AC = pointC - pointA;
	//based on the points find normal vector 
	normal = AB.Cross(AC);
	normal.Normalize();

	//count D to get the whole equation for plane
	d = normal.X * -pointB.X + normal.Y * -pointB.Y + normal.Z * -pointB.Z;
	//set the normal and D in a way for the upPoint to be above it;
	if (IsBelow(upPoint)) {
		normal *= -1;
		d *= -1;
	}
	//Now that we have the plane we can move it so that the particles arent going through it
	SetSphereRadiusVector(s);
	pointB -= sphereRadiusVec;
	//recalculate the D and thus move it
	d = normal.X * -pointB.X + normal.Y * -pointB.Y + normal.Z * -pointB.Z;
	//set max and mins
	SetMaxMinToINF();
}

void Wall::SetMaxMinToINF() {
	this->maxX = INFINITY;
	this->maxY = INFINITY;
	this->maxZ = INFINITY;
	this->minX = -INFINITY;
	this->minY = -INFINITY; 
	this->minZ = -INFINITY;
}

void Wall::SetMaxMin(double maxXi, double maxYi, double maxZi, double minXi, double minYi, double minZi) {
	this->maxX = maxXi;
	this->maxY = maxYi;
	this->maxZ = maxZi;
	this->minX = minXi;
	this->minY = minYi;
	this->minZ = minZi;
};

Wall::Wall(FVector normal, float d, float halfSph) {
	this->normal = normal;
	FVector point;
	//Find a point that lays in the plane
	if (normal.Z == 0) {
		if (normal.Y == 0) {
			point = FVector((-d) / normal.X,1,1);
		}
		else {
			point = FVector(1, (-normal.X - d) / normal.Y, 1);
		}
	}
	else {
		point = FVector(1, 1, (-normal.X - normal.Y - d)/normal.Z);
	}
	//Now that we have the point we can move the plane so that the particles arent going through it
	SetSphereRadiusVector(halfSph);
	point -= sphereRadiusVec;

	//Now that we have a point on the plane and we are recomputing D we can normalize the normal as it didnt have to be normalized before
	normal.Normalize();
	//recalculate the D and thus move it
	this->d = normal.X * -point.X + normal.Y * -point.Y + normal.Z * -point.Z;

	SetMaxMinToINF();
}

void Wall::SetSphereRadiusVector(double hS) {
	FVector n = normal;
	n.Normalize();
	sphereRadiusVec = n * hS;
}

FVector Wall::Bounce(FVector vel) {
	FVector n = normal;
	n.Normalize();
	//not very physically accurate but it will suffise
	FVector ret = vel - 2 * (vel.Dot(n)) * n;
	return ret;
}

bool Wall::IsBelow(FVector pos) {
	//find the point on the halfsphere that will be the first to touch the wall 
	pos -= sphereRadiusVec;
	float t = (pos.X) * normal.X + (pos.Y) * normal.Y + (pos.Z) * normal.Z + d;
	//check if the point is under or above the plane
	if (t < 0) {
		return true;
	}
	else {
		return false;
	}
}

FVector Wall::FindIntersection(FVector pos, FVector moveVector) {
	float dot = moveVector.Dot(-normal);
	//check the angle between of the moveVector and flipped normal to see if the wall should interact with the particle
	//Basically this means DO NOT BOUNCE from behined of the wall
	if (dot <= 0) {
		return FVector(INFINITY, INFINITY, INFINITY);
	}
	//find the point on the halfsphere that will be the first to touch the wall 
	pos -= sphereRadiusVec;
	//find the T parameter to get the itnersection ... just put the line into the plane equation and count t
	double t = (-d - pos.X * normal.X - pos.Y * normal.Y - pos.Z * normal.Z) / (normal.X * moveVector.X + normal.Y * moveVector.Y + normal.Z * moveVector.Z);
	//get the intersection from the line equation
	FVector intersection = pos + moveVector * t;
	float dist = (pos - intersection).Size();
	float velocitySize = moveVector.Size();
	//check if the intersection will happen with the next moveVector update
	if (dist <= velocitySize ) {
		return intersection + sphereRadiusVec;
	}
	return FVector(INFINITY, INFINITY, INFINITY);
}

bool Wall::ShouldInteract(FVector pos) {
	//if there is no intersection do not interact
	if (pos.X == INFINITY) {
		return false;
	}
	if (minX <= pos.X && pos.X <= maxX) {
		if (minY <= pos.Y && pos.Y <= maxY) {
			if (minZ <= pos.Z && pos.Z <= maxZ) {
				return true;
			}
		}
	}

	return false;
}

Wall::~Wall()
{
}