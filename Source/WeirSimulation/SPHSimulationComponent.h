// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Runtime/Core/Public/Async/ParallelFor.h"
#include "CoreMinimal.h"
#include "Wall.h"
#include "WaterParticle.h"
#include "WeirParams.h"
#include <string>  
#include "UObject/UObjectIterator.h"
#include "GameFramework/Actor.h"
#include "SPHSimulationComponent.generated.h"

UCLASS()
class WEIRSIMULATION_API ASPHSimulationComponent : public AActor
{
  GENERATED_BODY()

public:
  UPROPERTY(EditAnywhere, Category = "SPH Simulation")
	int scene_number = 0;
  // velikosti jezu => box
  // stary "jez"
	//float min_x = -270; 
	//float max_x = 410;
	//float min_y = 20;
	//float max_y = 200;
	//float min_z = 150;
	//float max_z = 500; 




	// Sets default values for this actor's properties
	ASPHSimulationComponent();
	UPROPERTY(EditAnywhere, Category = "SPH Simulation")
		float baseDensity = 128;
	UPROPERTY(EditAnywhere, Category = "SPH Simulation")
		float Viscosity = 400;
	UPROPERTY(EditAnywhere, Category = "SPH Simulation")
		float pressureCoef = 0.6;
	UPROPERTY(EditAnywhere, Category = "SPH Simulation")
		float h = 10.f;
	UPROPERTY(EditAnywhere, Category = "SPH Simulation")
		float mass = 4;
	UPROPERTY(EditAnywhere, Category = "SPH Simulation")
		float dampen;
	UPROPERTY(EditAnywhere, Category = "SPH Simulation");
		float sphereRadius = 8;

	UPROPERTY(EditAnywhere, Category = "SPH Simulation")
		float timeStep = 0.095;
	FVector box;
		
	UPROPERTY(EditAnywhere);
	  TSubclassOf<AActor> ActorToSpawn;
		/// <summary>
	/// spawner position
	/// </summary>
	FVector spawnPosition;
		/// <summary>
	/// spawner max position
	/// </summary>
	FVector spawnPositionMax;
	FTimerHandle CountdownTimerHandle;


protected:
	/// <summary>
	/// Set all the necesary parameters for the simulation
	/// </summary>
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	TArray<AWaterParticle*> particles;
	/// <summary>
	/// For given particle compute it's density
	/// </summary>
	/// <param name="index"> index of the particle to be solved</param>
	void ComputeDensity(int index);
	/// <summary>
	/// Compute the forces affecting a given particle 
	/// </summary>
	/// <param name="index">index of the current particle</param>
	void ComputeForce(int index);
	/// <summary>
	/// Update the position and velocity of a given particle
	/// </summary>
	/// <param name="index">index of the current particle</param>
	void Update(int index);
	/// <summary>
	/// Computes pressure force between two particles
	/// </summary>
	/// <param name="me">First particle that is currently being solved</param>
	/// <param name="other">The particle that is effecting the current particle</param>
	/// <param name="distance">Distance between them</param>
	/// <returns></returns>
	FVector PressureForce(AWaterParticle* me, AWaterParticle* other, float distance);
	/// <summary>
	/// Computes viscosity force between two particles 
	/// </summary>
	/// <param name="me">First particle that is currently being solved</param>
	/// <param name="other">The particle that is effecting the current particle</param>
	/// <param name="distance">Distance between them</param>
	/// <returns></returns>
	FVector ViscosityForce(AWaterParticle* me, AWaterParticle* other, float distance);
	/// <summary>
	/// Check which particles are out of bounds and remove them
	/// </summary>
	void DeleteOutOfBound();
	/// <summary>
	/// check if given given particle is within boundaries
	/// </summary>
	/// <param name="particle">index of the given particle</param>
	/// <returns></returns>
	bool CheckBoundary(FVector particle);
	/// <summary>
	/// Place the particles into the acceleration grid
	/// </summary>
	void Sort();
	/// <summary>
	/// Add new particles to the simulation
	/// </summary>
	void AddAllParticles();


	/// <summary>
	/// size of the gird 
	/// </summary>
	FVector gridsize;
	/// <summary>
	/// minimums of the box in which the simulation is being simulated
	/// </summary>
	FVector boxmins = FVector(0, 0, 0);
	/// <summary>
	/// The number to divided the poisiton by to get its location inside the grid
	/// </summary>
	FVector divider;
	/// <summary>
	/// Gravity 
	/// </summary>
	FVector gravity = FVector(0, 0, -9.8);
	/// <summary>
	/// Precomputed constant for the density kernel
	/// </summary>
	float densityConst;
	/// <summary>
	/// Precomputed constant for the forces kernel
	/// </summary>
	float ForceConst;
	/// <summary>
	/// h squared
	/// </summary>
	float h2;
	/// <summary>
	/// Acceleration grid
	/// </summary>
	TArray<TArray<TArray<TArray<AWaterParticle*>>>> grid;
	/// <summary>
	/// all the walls
	/// </summary>
	TArray<Wall> walls;

	WeirParams weirParams;

};

