// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "WeirSimulationGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class WEIRSIMULATION_API AWeirSimulationGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
