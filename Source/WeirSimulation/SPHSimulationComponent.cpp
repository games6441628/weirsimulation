// Fill out your copyright notice in the Description page of Project Settings.


#include "SPHSimulationComponent.h"
#include <chrono>
// Sets default values
ASPHSimulationComponent::ASPHSimulationComponent()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASPHSimulationComponent::BeginPlay()
{
	Super::BeginPlay();

	divider.X = h;
	divider.Y = h;
	divider.Z = h;

	weirParams.Init(scene_number);
	box = FVector(abs(weirParams.min_x - weirParams.max_x), abs(weirParams.min_y - weirParams.max_y), abs(weirParams.min_z - weirParams.max_z));
	walls = weirParams.walls;
	spawnPosition = weirParams.spawnPosition;
	spawnPositionMax = weirParams.spawnPositionMax;
	dampen = weirParams.dampen;


	gridsize = FVector(ceil(box.X / divider.X), ceil(box.Y / divider.Y), ceil(box.Z / divider.Z));

	//create new grid
	// IT is +2 because we create one more layer around tho whole grid so that we dont have to care about indexing when accessing it
	grid.SetNum(gridsize[0] + 2);
	for (int i = 0; i < gridsize[0] + 2; i++) {
		grid[i].SetNum(gridsize[1] + 2);
		for (int j = 0; j < gridsize[1] + 2; j++) {
			grid[i][j].SetNum(gridsize[2] + 2);
		}
	}

	boxmins = FVector(weirParams.min_x, weirParams.min_x, weirParams.min_z);

	//calculate the constants for the equations
	h2 = h * h;
	densityConst = (315.0) / (64 * PI * pow(h, 9.0));
	ForceConst = (45.0) / (PI * pow(h, 6.0));
	float halfSphere = sphereRadius;


	// spawn partickles every X-seconds
	AddAllParticles();
	GetWorldTimerManager().SetTimer(CountdownTimerHandle, this, &ASPHSimulationComponent::AddAllParticles, weirParams.spawnTime, true);

}

float frand(float max) {
	return static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / max));
}



void ASPHSimulationComponent::AddAllParticles(){

	if (particles.Num() < weirParams.maxAmount) {


	const FVector loc = spawnPosition;
	const FRotator rot = GetActorRotation();


	int XSize = std::abs(spawnPosition.X - spawnPositionMax.X); //x
	int YSize = std::abs(spawnPosition.Y - spawnPositionMax.Y); //y
	int ZSize = std::abs(spawnPosition.Z - spawnPositionMax.Z); //z

	
	int amountToSpawnX = weirParams.amountToSpawnX;
	int amountToSpawnY = weirParams.amountToSpawnY;
	int amountToSpawnZ = weirParams.amountToSpawnZ;

	int maxAmountToSpawnX = XSize / h;
	int maxAmountToSpawnY = YSize / h;
	int maxAmountToSpawnZ = ZSize / h;

	amountToSpawnX = std::min(amountToSpawnX, maxAmountToSpawnX);
	amountToSpawnY = std::min(amountToSpawnY, maxAmountToSpawnY);
	amountToSpawnZ = std::min(amountToSpawnZ, maxAmountToSpawnZ);

	float distX = (float)XSize / amountToSpawnX;
	float distY = (float)YSize / amountToSpawnY;
	float distZ = (float)ZSize / amountToSpawnZ;


	for (int x = 0; x < amountToSpawnX; x++) {
	  for (int y = 0; y < amountToSpawnY; y++) {
		for (int z = 0; z < amountToSpawnZ; z++) {

		  float xPos = x * distX;
		  float yPos = y * distY;

		  float zPos = distZ * z;
		  FVector newLoc = loc + FVector(xPos, yPos, zPos);
		  AWaterParticle* ActorRef = Cast<AWaterParticle>(GetWorld()->SpawnActor<AActor>(ActorToSpawn, newLoc, rot));
		  if (ActorRef != NULL) {
			  (ActorRef)->velocity = FVector(frand(10), frand(10), frand(10)) / 10;
			  particles.Push(ActorRef);
		  }
		}
	  }
	}
	}

}
// Called every frame
void ASPHSimulationComponent::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	DeleteOutOfBound();

	Sort();
	//compute densities for all the particles
	ParallelFor(particles.Num(), [this](int i) {
		ComputeDensity(i);
	});
	//compute forces for all the particles
	ParallelFor(particles.Num(), [this](int i) {
		ComputeForce(i);
	});
	// update all particles
	ParallelFor(particles.Num(), [this](int i) {
		Update(i);
	});
}


void ASPHSimulationComponent::Update(int i) {
	AWaterParticle* me = particles[i];
	FVector previousPosition = me->position;

	me->velocity += timeStep * me->forces;
	me->position += timeStep * me->velocity;

	//check colisions with walls
	FVector newVel;
	for (Wall wall : walls) {
	// check if the particle was previously behined me so that I bounce from behined of the wall
	if (!wall.IsBelow(previousPosition)) {
		//check if particle is below (Quick test to eliminate most of the necesarry counting)
		if (wall.IsBelow(me->position)) {
			FVector inter = wall.FindIntersection(me->position, me->velocity * timeStep);
			if (wall.ShouldInteract(inter)) {
				me->position -= timeStep * me->velocity;
				newVel = wall.Bounce(me->velocity);
				if (newVel.Size() > 20) { newVel = newVel * dampen; }
				me->velocity = newVel;
			}
		}
	  }
	}
}



void ASPHSimulationComponent::ComputeDensity(int index) {
	float sum = 0;
	AWaterParticle* me = particles[index];
	int ind0 = me->indexes[0];
	int ind1 = me->indexes[1];
	int ind2 = me->indexes[2];
	// Iterate over all of the cells in the proximity of this cell
	for (int x = ind0 - 1; x <= ind0 + 1; x++) {
		for (int y = ind1 - 1; y <= ind1 + 1; y++) {
			for (int z = ind2 - 1; z <= ind2 + 1; z++) {
				// Iterate through each and every particle in the cell
				for (int i = 0; i < grid[x][y][z].Num(); i++) {
					AWaterParticle* other = grid[x][y][z][i];
					float distance = me->position.Dist(me->position, other->position);
					if (distance <= h) {
						sum +=  pow(h2 - distance * distance, 3.0);
					}
				}
			}
		}
	}
	//Update density and pressure
	particles[index]->density = mass * densityConst * sum;
	particles[index]->pressure = pressureCoef * (me->density - baseDensity);
}

void ASPHSimulationComponent::ComputeForce(int index) {
	FVector pressF(0, 0, 0);
	FVector viscF(0, 0, 0);
	AWaterParticle* me = particles[index];
	int ind0 = me->indexes[0];
	int ind1 = me->indexes[1];
	int ind2 = me->indexes[2];
	// Iterate over all of the cells in the proximity of this cell
	for (int x = ind0 - 1; x <= ind0 + 1; x++) {
		for (int y = ind1 - 1; y <= ind1 + 1; y++) {
			for (int z = ind2 - 1; z <= ind2 + 1; z++) {
				// Iterate through each and every particle in the cell
				for (int i = 0; i < grid[x][y][z].Num(); i++) {
					AWaterParticle* other = grid[x][y][z][i];
					if (other->position != me->position) {
						float distance = me->position.Dist(me->position, other->position);
						if (distance <= h) {
							viscF += ViscosityForce(me, other, distance);
							pressF += PressureForce(me, other, distance);

						}
					}

				}
			}
		}
	}
	pressF = ForceConst * (mass / (me->density)) * pressF;
	viscF = ForceConst *  mass * Viscosity * viscF;
	particles[index]->forces = (gravity*mass+ pressF + viscF) / mass;
}


FVector ASPHSimulationComponent::PressureForce(AWaterParticle* me, AWaterParticle* other, float distance) {
	FVector sum(0, 0, 0);

	FVector dist_vec = other->position - me->position;

	FVector spike = pow(h - distance, 2.0f) * dist_vec / (distance);
	sum += (me->pressure + other->pressure) * spike / (2 * other->density);

	return mass * sum;
}

FVector ASPHSimulationComponent::ViscosityForce(AWaterParticle* me, AWaterParticle* other, float distance) {
	FVector sum(0, 0, 0);
	double spike =  (h - distance);
	sum = (other->velocity - me->velocity) / other->density * spike;
	return mass * sum;
}


void ASPHSimulationComponent::DeleteOutOfBound() {
  TArray<AWaterParticle*> particles_COPY;

  for (auto item : particles) {
	if (CheckBoundary(item->position)) {
	  item->Destroy();
	}
	else {
	  particles_COPY.Add(item);
	}
  }
  particles.Empty();
  for (auto item : particles_COPY) {
	particles.Add(item);
  }

}
bool ASPHSimulationComponent::CheckBoundary(FVector particle) {

  int wallThick = 20;
  int x_offset = 10;
  if (particle.X < weirParams.min_x - wallThick || particle.X > weirParams.max_x + wallThick + x_offset|| particle.Y < weirParams.min_y - wallThick || particle.Y > weirParams.max_y + wallThick || particle.Z < weirParams.min_z - wallThick) {
	return true;
  }
  else {
	return false;
  }

}
void ASPHSimulationComponent::Sort() {
	//Clear the grid
	for (size_t x = 1; x < gridsize[0] + 1; x++) {
		for (size_t y = 1; y < gridsize[1] + 1; y++) {
			for (size_t z = 1; z < gridsize[2] + 1; z++) {
				grid[x][y][z].Empty();
			}
		}
	}
	//compute new positions of the particles
	for (size_t i = 0; i < particles.Num(); i++) {
		FVector pos = particles[i]->position;

		int x = floor(pos.X - boxmins.X) / divider.X;
		int y = floor(pos.Y - boxmins.Y) / divider.Y;
		int z = floor(pos.Z - boxmins.Z) / divider.Z;
		//round those out of the bounds to the closest cell
		if (x >= gridsize[0]) {
			x = gridsize[0] - 1;
		}
		if (y >= gridsize[1]) {
			y = gridsize[1] - 1;
		}
		if (z >= gridsize[2]) {
			z = gridsize[2] - 1;
		}
		if (x < 1) {
			x = 1;
		}
		if (y < 1) {
			y = 1;
		}
		if (z < 1) {
			z = 1;
		}
		//Add the particle to the appropriate cell
		grid[x + 1][y + 1][z + 1].Add(particles[i]);
		particles[i]->indexes = FVector(x + 1, y + 1, z + 1);
	}
}

