// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ParticleSpawner.generated.h"

UCLASS()
class WEIRSIMULATION_API AParticleSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AParticleSpawner();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// to enable editing in menu
	UPROPERTY(EditAnywhere);
	// actor to spawn, chosen in the blueprint. (waterPartilceWithMesh)
	TSubclassOf<AActor> ActorToSpawn;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
