// Fill out your copyright notice in the Description page of Project Settings.


#include "WaterParticle.h"

// Sets default values
AWaterParticle::AWaterParticle()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AWaterParticle::BeginPlay()
{
	Super::BeginPlay();
	position = GetActorLocation();
}

// Called every frame
void AWaterParticle::Tick(float DeltaTime)
{
	//Super::Tick(DeltaTime);
	// 
	//just to see, if particle can move
	//srand((unsigned)time(NULL));
	//FVector curr_loc = GetActorLocation();
	//FVector loc = FVector(-5 + (rand() % 5), -5 + (rand() % 5), 0 + (rand() % 5));
	//FVector result = loc + curr_loc;
	SetActorLocation(position);

}

