// Fill out your copyright notice in the Description page of Project Settings.


#include "ParticleSpawner.h"

// Sets default values
AParticleSpawner::AParticleSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AParticleSpawner::BeginPlay()
{
	Super::BeginPlay();
	
	//Spawn here
	const FVector loc = GetActorLocation();
	const FRotator rot = GetActorRotation();

	//size of the pool
	//int width = 1120;
	//int height = 910;
	int width = 500;
	int height = 200;
	//int ballSize = 0.08 * 1000;

	int amountToSpawn = 25;
	int amountOfLevels = 4;

	float distW = (float)width / amountToSpawn;
	float distH = (float)height / amountToSpawn;


	for (int i = 0; i < amountToSpawn; i++) {
	  for (int j = 0; j < amountToSpawn; j++) {
		for (int u = 0; u < amountOfLevels+1; u++) {

		  float iPos = i * distW;
		  float jPos = j* distH;

		  float uPos = distW * u;
		  FVector newLoc = loc + FVector(iPos, jPos, uPos);
		  AActor* ActorRef = GetWorld()->SpawnActor<AActor>(ActorToSpawn, newLoc, rot);
		}
	  }
	}


}

// Called every frame
void AParticleSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

