// Fill out your copyright notice in the Description page of Project Settings.


#include "WeirParams.h"

WeirParams::WeirParams()
{
}

WeirParams::~WeirParams()
{
  walls.Empty();
}

void WeirParams::Init(int scene_num) {

  if (scene_num == 0) {
	Scene_1();

  }
  else if (scene_num == 1) {
	Scene_2();

  }
  else if (scene_num == 2) {
	Scene_3();

  }
}
void WeirParams::Scene_1() {

	amountToSpawnX = 20;
	amountToSpawnY = 20;
	amountToSpawnZ = 3;
	spawnTime = 10;
	maxAmount = 6000;

  dampen = 0.2;

  min_x = 0;
  max_x = 620;
  min_y = -204;
  max_y = 0;
  min_z = 40;
  max_z = 300;

  spawnPosition = FVector(-9, -203, 220);
  spawnPositionMax = FVector(176, -1, 300);
  // upper bottom
  Wall wallbottom = Wall(FVector(0, 0, 1), -(150), halfSphere);
  wallbottom.SetMaxMin(202, INFINITY, INFINITY, -INFINITY, -INFINITY, -INFINITY);
  wallbottom.SetSphereRadiusVector(halfSphere);
  walls.Push(wallbottom);

  //lower bottom
  Wall wallbottom2 = Wall(FVector(0, 0, 1), -40, halfSphere);
  walls.Push(wallbottom2);


  Wall wallleft = Wall(FVector(0, 1, 0), -min_y, halfSphere);
  Wall wallright = Wall(FVector(0, -1, 0), max_y, halfSphere);
  Wall wallfront = Wall(FVector(-1, 0, 0), max_x, halfSphere);
  Wall wallback = Wall(FVector(1, 0, 0), -min_x, halfSphere);

  //walls.Push(wallfront);
  walls.Push(wallleft);
  walls.Push(wallright);
  walls.Push(wallback);


  ////right part of wall
  Wall wallWeirRight = Wall(FVector(-1, 0, 0), 175, halfSphere);
  wallWeirRight.SetMaxMin(INFINITY, -160, INFINITY, -INFINITY, -INFINITY, -INFINITY);
  walls.Push(wallWeirRight);

  ////other side of the same wall
  Wall wallWeirRight2 = Wall(FVector(1, 0, 0), -(207), halfSphere);
  wallWeirRight2.SetMaxMin(INFINITY, -160, INFINITY, -INFINITY, -INFINITY, -INFINITY);
  walls.Push(wallWeirRight2);

  Wall wallWierRightMiddle = Wall(FVector(0, 1, 0), -(-157), halfSphere);
  wallWierRightMiddle.SetMaxMin(203, INFINITY, INFINITY, 180, -INFINITY, -INFINITY);
  walls.Push(wallWierRightMiddle);

  Wall wallWierRightSAFEMiddle = Wall(FVector(0, 1, 0), -(-157 - halfSphere), halfSphere);
  wallWierRightSAFEMiddle.SetMaxMin(203, INFINITY, INFINITY, 180, -INFINITY, -INFINITY);
  walls.Push(wallWierRightSAFEMiddle);


  ////right part of wall
  Wall wallWeirLeft = Wall(FVector(-1, 0, 0), 175, halfSphere);
  wallWeirLeft.SetMaxMin(INFINITY, INFINITY, INFINITY, -INFINITY, -50, -INFINITY);
  walls.Push(wallWeirLeft);

  ////other side of the same wall
  Wall wallWeirLeft2 = Wall(FVector(1, 0, 0), -207, halfSphere);
  wallWeirLeft.SetMaxMin(INFINITY, INFINITY, INFINITY, -INFINITY, -50, -INFINITY);
  walls.Push(wallWeirLeft2);

  Wall wallWierLeftMiddle = Wall(FVector(0, -1, 0), -48, halfSphere);
  wallWierLeftMiddle.SetMaxMin(203, INFINITY, INFINITY, 180, -INFINITY, -INFINITY);
  walls.Push(wallWierLeftMiddle);

  Wall wallWierLeftSAFEMiddle = Wall(FVector(0, -1, 0), -48 + halfSphere, halfSphere);
  wallWierLeftSAFEMiddle.SetMaxMin(203, INFINITY, INFINITY, 180, -INFINITY, -INFINITY);
  walls.Push(wallWierLeftSAFEMiddle);

  //607
  Wall wallWierback = Wall(FVector(-1, 0, 0), 607, halfSphere);
  wallWierback.SetMaxMin(INFINITY, INFINITY, 69, -INFINITY, -INFINITY, -INFINITY);
  walls.Push(wallWierback);

  Wall wallWierBackstraight = Wall(FVector(0, 0, 1), -69, halfSphere);
  wallWierBackstraight.SetMaxMin(639, INFINITY, INFINITY, 607, -INFINITY, -INFINITY);
  walls.Push(wallWierBackstraight);
}
void WeirParams::Scene_2() {

	amountToSpawnX = 20;
	amountToSpawnY = 20;
	amountToSpawnZ = 3;
	spawnTime = 10;
	maxAmount = 6000;

  dampen = 0.2;

  min_x = 0;
  max_x = 645;
  min_y = -204;
  max_y = 0;
  min_z = 40;
  max_z = 300;

  spawnPosition = FVector(-9, -203, 220);
  spawnPositionMax = FVector(176, -1, 300);
  // upper bottom
  Wall wallbottom = Wall(FVector(0, 0, 1), -(195), halfSphere);
  wallbottom.SetMaxMin(198, INFINITY, INFINITY, -INFINITY, -INFINITY, -INFINITY);
  wallbottom.SetSphereRadiusVector(halfSphere);
  walls.Push(wallbottom);

  //lower bottom
  Wall wallbottom2 = Wall(FVector(0, 0, 1), -30, halfSphere);
  walls.Push(wallbottom2);


  Wall wallleft = Wall(FVector(0, 1, 0), -min_y, halfSphere);
  Wall wallright = Wall(FVector(0, -1, 0), max_y, halfSphere);
  Wall wallfront = Wall(FVector(-1, 0, 0), max_x, halfSphere);
  Wall wallback = Wall(FVector(1, 0, 0), -min_x, halfSphere);

  //walls.Push(wallfront);
  walls.Push(wallleft);
  walls.Push(wallright);
  walls.Push(wallback);


  ////right part of wall
  Wall wallWeirRight = Wall(FVector(-1, 0, 0), 175, halfSphere);
  wallWeirRight.SetMaxMin(INFINITY, -160, INFINITY, -INFINITY, -INFINITY, -INFINITY);
  walls.Push(wallWeirRight);

  ////other side of the same wall
  Wall wallWeirRight2 = Wall(FVector(1, 0, 0), -(207), halfSphere);
  wallWeirRight2.SetMaxMin(INFINITY, -160, INFINITY, -INFINITY, -INFINITY, -INFINITY);
  walls.Push(wallWeirRight2);

  Wall wallWierRightMiddle = Wall(FVector(0, 1, 0), -(-157), halfSphere);
  wallWierRightMiddle.SetMaxMin(203, INFINITY, INFINITY, 180, -INFINITY, -INFINITY);
  walls.Push(wallWierRightMiddle);

  Wall wallWierRightSAFEMiddle = Wall(FVector(0, 1, 0), -(-157 - halfSphere), halfSphere);
  wallWierRightSAFEMiddle.SetMaxMin(203, INFINITY, INFINITY, 180, -INFINITY, -INFINITY);
  walls.Push(wallWierRightSAFEMiddle);


  ////right part of wall
  Wall wallWeirLeft = Wall(FVector(-1, 0, 0), 175, halfSphere);
  wallWeirLeft.SetMaxMin(INFINITY, INFINITY, INFINITY, -INFINITY, -50, -INFINITY);
  walls.Push(wallWeirLeft);

  ////other side of the same wall
  Wall wallWeirLeft2 = Wall(FVector(1, 0, 0), -207, halfSphere);
  wallWeirLeft.SetMaxMin(INFINITY, INFINITY, INFINITY, -INFINITY, -50, -INFINITY);
  walls.Push(wallWeirLeft2);

  Wall wallWierLeftMiddle = Wall(FVector(0, -1, 0), -48, halfSphere);
  wallWierLeftMiddle.SetMaxMin(203, INFINITY, INFINITY, 180, -INFINITY, -INFINITY);
  walls.Push(wallWierLeftMiddle);

  Wall wallWierLeftSAFEMiddle = Wall(FVector(0, -1, 0), -48 + halfSphere, halfSphere);
  wallWierLeftSAFEMiddle.SetMaxMin(203, INFINITY, INFINITY, 180, -INFINITY, -INFINITY);
  walls.Push(wallWierLeftSAFEMiddle);

  //607
  Wall wallWierback = Wall(FVector(-1, 0, 0), 651, halfSphere);
  wallWierback.SetMaxMin(INFINITY, INFINITY, 51, -INFINITY, -INFINITY, -INFINITY);
  walls.Push(wallWierback);

  Wall wallWierBackstraight = Wall(FVector(0, 0, 1), -51, halfSphere);
  wallWierBackstraight.SetMaxMin(680, INFINITY, INFINITY, 645, -INFINITY, -INFINITY);
  walls.Push(wallWierBackstraight);

  Wall shierWall = Wall(FVector(198, -54, 183), FVector(198, -55, 183), FVector(270, -54, 21), FVector(249, -40, 125), halfSphere);
  shierWall.SetMaxMin(INFINITY, INFINITY, INFINITY, 195, -INFINITY, -INFINITY);
  walls.Push(shierWall);
}

void WeirParams::Scene_3() {

	amountToSpawnX = 8;
	amountToSpawnY = 15;
	amountToSpawnZ = 2;
	spawnTime = 5;
	maxAmount = 2500;


    dampen = 0.2;

	//min_x = -270; 
	//max_x = 410;
	//min_y = 20;
	//max_y = 200;
	//min_z = 150;
	//max_z = 500; 

	min_x = -9;
	max_x = 470;
	min_y = -180;
	max_y = 0;
	min_z = 62;
	max_z = 350;

	spawnPosition = FVector(-5, -170, 218);
	spawnPositionMax = FVector(80, 5, 250);
  // upper bottom
	Wall wallup = Wall(FVector(-10, -180, 170), FVector(-10, 0, 170), FVector(126, 0, 155), FVector(50, 0, 175), halfSphere);
	wallup.SetMaxMin(148, INFINITY, INFINITY, -INFINITY, -INFINITY, -INFINITY);
	walls.Push(wallup);

	
	Wall wallshier = Wall(FVector(144, 0, 147), FVector(144, -70, 147), FVector(219, -70, 77), FVector(200,-20, 150), halfSphere);
	//Wall wall = Wall(FVector(23, 209, 275), FVector(23, 241, 275), FVector(5, 241, 235), FVector(5, 241, 250), halfSphere);
	wallshier.SetMaxMin(INFINITY, INFINITY, INFINITY, 142, -INFINITY, -INFINITY);
	walls.Push(wallshier);


	Wall wallbottom = Wall(FVector(219, -70, 77), FVector(219, 0, 77), FVector(445, -70, 62), FVector(0, 130, 300), halfSphere);
	walls.Push(wallbottom);


	Wall wallleft = Wall(FVector(0, 1, 0), -min_y, halfSphere);
	Wall wallright = Wall(FVector(0, -1, 0), max_y, halfSphere);
	

	Wall wallback = Wall(FVector(1, 0, 0), -min_x, halfSphere);

	Wall wallfront = Wall(FVector(-1, 0, 0), max_x - 10, halfSphere);
	wallfront.SetMaxMin(INFINITY, INFINITY, 75, -INFINITY, -INFINITY, -INFINITY);

	Wall wallhelp = Wall(FVector(0, 0, 1), -75, halfSphere);
	wallhelp.SetMaxMin(max_x + 10, INFINITY, INFINITY, max_x - 20, -INFINITY, -INFINITY);

	walls.Push(wallhelp);
	walls.Push(wallfront);
	walls.Push(wallleft);
	walls.Push(wallright);
	walls.Push(wallback);
	 
}