// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WaterParticle.generated.h"

UCLASS()
class WEIRSIMULATION_API AWaterParticle : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWaterParticle();
	FVector position;
	FVector indexes;
	FVector velocity;
	float density;
	float pressure;
	FVector forces;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
