// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Wall.h"
#include "CoreMinimal.h"

/**
 * 
 */
class WEIRSIMULATION_API WeirParams
{
public:
	WeirParams();
	~WeirParams();

	float halfSphere = 8;
	FVector spawnPosition ;
	FVector spawnPositionMax;
	float dampen = 0.99;

	int amountToSpawnX;
	int amountToSpawnY;
	int amountToSpawnZ;

	int spawnTime;
	int maxAmount;

	float min_x = 0;
	float max_x = 0;
	float min_y = 4;
	float max_y = 0;
	float min_z = 0;
	float max_z = 0;
	TArray<Wall> walls;
	void Init(int scene_num);
	//big straight weir
	void Scene_1();
	//big shier weir
	void Scene_2();
	//small old weir
	void Scene_3();
};
