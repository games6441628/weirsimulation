# Smoothed-particle hydrodynamics simulation



## Introduction
This project focuses on simulating liquid behavior in a three-dimensional space using the Smoothed-Particle Hydrodynamics (SPH) method. SPH is a computational technique that calculates the velocity and position of individual particles independently, placing it within the framework of Lagrange's group. The implementation of this project involves creating a CPU-based simulation using C++ within the Unreal Engine. This collaborative effort is being undertaken in partnership with Petr Varga.

## Showcase
A demonstration illustrating the functionality of SPH implementation.

<img src="./weir1.PNG" width="400">
<img src="./weir2.PNG" width="400">
<img src="./weir3.PNG" width="400">
